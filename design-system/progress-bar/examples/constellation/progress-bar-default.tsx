import React from 'react';

import ProgressBar from '../../src';

const ProgressBarDefaultExample = () => {
  return <ProgressBar value={0.4} />;
};

export default ProgressBarDefaultExample;
