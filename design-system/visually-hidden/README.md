# VisuallyHidden

A composable element that hides elements from the screen while keeping them accessible.

## Usage

`import VisuallyHidden from '@atlaskit/visually-hidden';`
