# FocusRing

A focus ring is used to apply accessibile styles to a focusable element.

## Usage

`import FocusRing from '@atlaskit/focus-ring';`
