# @atlaskit/focus-ring

## 0.1.1

### Patch Changes

- [`9c9296f2959`](https://bitbucket.org/atlassian/atlassian-frontend/commits/9c9296f2959) - Fix bug where the package was being exported from the wrong file.

## 0.1.0

### Minor Changes

- [`5ab09801cfa`](https://bitbucket.org/atlassian/atlassian-frontend/commits/5ab09801cfa) - [ux] Updates focus-ring to use an offset box-shadow for its focus state.
- [`adaa7913de0`](https://bitbucket.org/atlassian/atlassian-frontend/commits/adaa7913de0) - Initial release for the package. A Focus Ring can be used to compose focusable elements with a simple composable API.

### Patch Changes

- Updated dependencies

## 0.0.4

### Patch Changes

- [`229b32842b5`](https://bitbucket.org/atlassian/atlassian-frontend/commits/229b32842b5) - Fix .npmignore and tsconfig.json for **tests**

## 0.0.3

### Patch Changes

- [`d3265f19be`](https://bitbucket.org/atlassian/atlassian-frontend/commits/d3265f19be) - Transpile packages using babel rather than tsc

## 0.0.2

### Patch Changes

- [`5f58283e1f`](https://bitbucket.org/atlassian/atlassian-frontend/commits/5f58283e1f) - Export types using Typescript's new "export type" syntax to satisfy Typescript's --isolatedModules compiler option.
  This requires version 3.8 of Typescript, read more about how we handle Typescript versions here: https://atlaskit.atlassian.com/get-started
  Also add `typescript` to `devDependencies` to denote version that the package was built with.

## 0.0.1

### Patch Changes

- [`b443b5a60f`](https://bitbucket.org/atlassian/atlassian-frontend/commits/b443b5a60f) - Renamed template package
