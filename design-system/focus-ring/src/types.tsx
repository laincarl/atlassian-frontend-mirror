import type { ReactElement } from 'react';

export type FocusRingProps = {
  /**
   * The focusable element to be rendered within the `FocusRing`.
   */
  children: ReactElement;
};
