export {
  mockProductRecommendationsEndpoint,
  mockAvailableProductsEndpoint,
  mockEndpoints,
  REQUEST_SLOW,
  REQUEST_MEDIUM,
  REQUEST_FAST,
  getMockData,
  availableProductsUrlRegex,
  productRecommendationsUrlRegex,
} from './mocks/mock-endpoints';
export type { DataTransformer } from './mocks/mock-endpoints';
